/*!
 * \page using_agrum How to use aGrUM with CMake
 *
 * As a build system, aGrUM uses CMake (http://www.cmake.org). A minimal project
 * with
 * agrum should look like this (for a project foo):
 * - in the project folder, a sub-folder src,
 * - in src folder, your *.{cpp|h|etc.} files
 * - in src folder, a file named CMakeLists.txt like this one :
 *
 * \code{.cmake}
project(FOO)
cmake_minimum_required(VERSION 2.8)
# classical options 
# -DCMAKE_BUILD_TYPE=Debug|Release 
# -G "MinGW Makefiles" 

# cmake -DCMAKE_BUILD_TYPE=Debug
# or
# cmake -DCMAKE_BUILD_TYPE=Release
#     Release is the default option (thanks to the next lines)

set (CMAKE_CXX_STANDARD 14)

find_package(OpenMP)
if (OPENMP_FOUND)
  set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
  set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
  set (CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
endif()

# do not forget to change this line if needed ("act install -d/folder/install/agrum.")
set(AGRUM_INSTALLATION_DIRECTORY "/folder/install/agrum")

set(aGrUM_DIR "${AGRUM_INSTALLATION_DIRECTORY}/lib/cmake/aGrUM/")
find_package(aGrUM)

if (aGrUM_FOUND)
  include(${AGRUM_USE_FILE})
else (aGrUM_FOUND)
  message(FATAL_ERROR "Please install aGrUM")
endif (aGrUM_FOUND)

file(GLOB FOO_SOURCE ${FOO_SOURCE_DIR}/*.cpp)
file(GLOB FOO_INCLUDE ${FOO_SOURCE_DIR}/*.h)

add_executable (foo ${FOO_SOURCE})
target_link_libraries(foo agrum)

 * \endcode
 *
 * - a small minimum src/FOO.cpp :
 * \code{.cpp}
#include <iostream>

#include <agrum/tools/core/hashTable.h>

int main() {
  gum::HashTable<std::string,int> h;

  h.insert("Hello",1);
  h.insert("World",2);

  std::cout<<h<<std::endl;
}
 * \endcode
 *
 * - to compile the project (from the project folder)
 *
 * \code{.sh}
 *     mkdir build
 *     cd build
 *     cmake ../src/
 *     make
 * \endcode
 *
 * - build/foo is the executable.
 *
 */
