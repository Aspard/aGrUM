%feature("docstring") BNDatabaseGenerator
"
BNDatabaseGenerator is used to easily generate databases from a gum.BayesNet.

BNDatabaseGenerator(bn) -> BNDatabaseGenerator
    Parameters:
        * **bn** (*gum.BayesNet*) -- the Bayesian network used to generate data.
"
