Bayesian network as classifier
===============================

.. image:: _static/classifiers.png

.. automodule:: pyAgrum.lib.classifier
    :members:
    :noindex: