Other functions from aGrUM
==========================

.. toctree::
  :maxdepth: 2
  :titlesonly:

  listeners

Random functions
----------------

.. autofunction:: pyAgrum.initRandom

.. autofunction:: pyAgrum.randomProba

.. autofunction:: pyAgrum.randomDistribution


OMP functions
-------------

.. autofunction:: pyAgrum.isOMP

.. autofunction:: pyAgrum.setNumberOfThreads

.. autofunction:: pyAgrum.getNumberOfLogicalProcessors

.. autofunction:: pyAgrum.getMaxNumberOfThreads

